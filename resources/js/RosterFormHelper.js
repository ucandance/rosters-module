function initRosterFormHelper() {
	document.querySelector("div.year-field input").value = new Date().getFullYear();
	document.querySelector("div.commit_message-field textarea").value = "";
}
window.addEventListener("load", initRosterFormHelper);
