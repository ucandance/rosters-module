<?php

return [
    'rosters' => [
        'read',
        'write',
        'delete',
    ],
    'nights' => [
        'read',
        'write',
        'delete',
    ],
];
