<?php

return [
	"name.name" => "Roster Name",
	"slug.name" => "Slug",
	"year.name" => "Year",
	"nights.name" => "Classes",
	"night.name" => "Class",
	"door_1.name" => "Door",
	"door_2.name" => "Door",
	"sound_1.name" => "Sound",
	"sound_2.name" => "Sound",
	"mj_1.name" => "MJ Teacher",
	"mj_2.name" => "MJ Offsider",
	"latin_1.name" => "Latin Teacher",
	"latin_2.name" => "Latin Offsider",
	"commit_message.name" => "Describe What Changed",
];
