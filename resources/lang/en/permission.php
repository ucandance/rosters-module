<?php

return [
    'rosters' => [
        'name'   => 'Rosters',
        'option' => [
            'read'   => 'Can read rosters?',
            'write'  => 'Can create/edit rosters?',
            'delete' => 'Can delete rosters?',
        ],
    ],
    'nights' => [
        'name'   => 'Nights',
        'option' => [
            'read'   => 'Can read nights?',
            'write'  => 'Can create/edit nights?',
            'delete' => 'Can delete nights?',
        ],
    ],
];
