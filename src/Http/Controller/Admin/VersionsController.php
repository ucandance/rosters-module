<?php namespace Finnito\RostersModule\Http\Controller\Admin;

use Finnito\RostersModule\Roster\RosterModel;

/**
 * Class VersionsController
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class VersionsController extends \Anomaly\Streams\Platform\Http\Controller\VersionsController
{

   /**
     * The versionable model.
     *
     * @var string
     */
    protected $model = RosterModel::class;
}
