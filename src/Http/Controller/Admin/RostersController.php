<?php namespace Finnito\RostersModule\Http\Controller\Admin;

use Finnito\RostersModule\Roster\Form\RosterFormBuilder;
use Finnito\RostersModule\Roster\Table\RosterTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

/**
 * Class RostersController
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class RostersController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param RosterTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(RosterTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param RosterFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(RosterFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param RosterFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(RosterFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
