<?php namespace Finnito\RostersModule\Http\Controller\Admin;

use Finnito\RostersModule\Night\Form\NightFormBuilder;
use Finnito\RostersModule\Night\Table\NightTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

/**
 * Class NightsController
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param NightTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(NightTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param NightFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(NightFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param NightFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(NightFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
