<?php namespace Finnito\RostersModule\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Finnito\RostersModule\Roster\Contract\RosterRepositoryInterface;
use Finnito\RostersModule\Roster\Command\AddTemplateEditLink;
use Illuminate\Contracts\Auth\Guard;

/**
 * Class RostersController
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class RostersController extends PublicController
{

    /**
     * Show current or next roster
     *
     * @param RosterRepositoryInterface $rosters
     * @param Guard $auth
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function current(RosterRepositoryInterface $rosters, Guard $auth)
    {
        if (!$user = $auth->user()) {
            redirect('/login?redirect='.url()->previous())->send();
        }
        
        if (!$user->hasAnyRole(["admin", "committee", "volunteer"])) {
            abort(403);
        }
        $currentRoster = $rosters->current();
        if (!is_null($currentRoster)) {
            $this->dispatch(new AddTemplateEditLink($currentRoster));
            $this->template->set("meta_title", "Roster: ".$currentRoster->name);
        }
        $this->breadcrumbs->add(
            "Roster",
            "/roster"
        );
        return $this->view->make(
            "finnito.module.rosters::roster",
            [
                "rosters" => $rosters->all(),
                "roster" => $currentRoster
            ]
        );
    }

    /**
     * Show a roster by a given slug
     *
     * @param RosterRepositoryInterface $rosters
     * @param Guard $auth
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function single(RosterRepositoryInterface $rosters, Guard $auth, $slug)
    {
        if (!$user = $auth->user()) {
            redirect('/login?redirect='.url()->previous())->send();
        }

        if (!$user->hasAnyRole(["admin", "committee", "volunteer"])) {
            abort(403);
        }

        if (!$roster = $rosters->findBySlug($slug)) {
            abort(404);
        }
        $this->template->set("meta_title", "Roster: ".$roster->name);
        $this->breadcrumbs->add(
            "Roster",
            "/roster"
        );
        $this->breadcrumbs->add(
            $roster->name,
            "/roster/{$roster->name}"
        );
        $this->template->set('edit_link', "/admin/rosters/edit/".$roster->id);
        $this->template->set("edit_type", $roster->name);
        return $this->view->make(
            "finnito.module.rosters::roster",
            [
                "rosters" => $rosters->all(),
                "roster" => $roster
            ]
        );
    }
}
