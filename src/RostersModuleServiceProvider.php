<?php namespace Finnito\RostersModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Finnito\RostersModule\Night\Contract\NightRepositoryInterface;
use Finnito\RostersModule\Night\NightRepository;
use Anomaly\Streams\Platform\Model\Rosters\RostersNightsEntryModel;
use Finnito\RostersModule\Night\NightModel;
use Finnito\RostersModule\Roster\Contract\RosterRepositoryInterface;
use Finnito\RostersModule\Roster\RosterRepository;
use Anomaly\Streams\Platform\Model\Rosters\RostersRostersEntryModel;
use Finnito\RostersModule\Roster\RosterModel;
use Illuminate\Routing\Router;

// Versioning
use Anomaly\PagesModule\Http\Controller\Admin\VersionsController;
use Anomaly\Streams\Platform\Assignment\VersionRouter;

/**
 * Class RostersModuleServiceProvider
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class RostersModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Additional addon plugins.
     *
     * @type array|null
     */
    protected $plugins = [];

    /**
     * The addon Artisan commands.
     *
     * @type array|null
     */
    protected $commands = [];

    /**
     * The addon's scheduled commands.
     *
     * @type array|null
     */
    protected $schedules = [];

    /**
     * The addon API routes.
     *
     * @type array|null
     */
    protected $api = [];

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        'admin/rosters/nights'           => 'Finnito\RostersModule\Http\Controller\Admin\NightsController@index',
        'admin/rosters/nights/create'    => 'Finnito\RostersModule\Http\Controller\Admin\NightsController@create',
        'admin/rosters/nights/edit/{id}' => 'Finnito\RostersModule\Http\Controller\Admin\NightsController@edit',
        'admin/rosters'           => 'Finnito\RostersModule\Http\Controller\Admin\RostersController@index',
        'admin/rosters/create'    => 'Finnito\RostersModule\Http\Controller\Admin\RostersController@create',
        'admin/rosters/edit/{id}' => 'Finnito\RostersModule\Http\Controller\Admin\RostersController@edit',

        // "admin/rosters/versions" => "Finnito\RostersModule\Http\Controller\Admin\VersionsController",

        // Public pages
        "roster" => "Finnito\RostersModule\Http\Controller\RostersController@current",
        "roster/{slug}" => "Finnito\RostersModule\Http\Controller\RostersController@single",
    ];

    /**
     * The addon middleware.
     *
     * @type array|null
     */
    protected $middleware = [
        //Finnito\RostersModule\Http\Middleware\ExampleMiddleware::class
    ];

    /**
     * Addon group middleware.
     *
     * @var array
     */
    protected $groupMiddleware = [
        //'web' => [
        //    Finnito\RostersModule\Http\Middleware\ExampleMiddleware::class,
        //],
    ];

    /**
     * Addon route middleware.
     *
     * @type array|null
     */
    protected $routeMiddleware = [];

    /**
     * The addon event listeners.
     *
     * @type array|null
     */
    protected $listeners = [
        //Finnito\RostersModule\Event\ExampleEvent::class => [
        //    Finnito\RostersModule\Listener\ExampleListener::class,
        //],
    ];

    /**
     * The addon alias bindings.
     *
     * @type array|null
     */
    protected $aliases = [
        //'Example' => Finnito\RostersModule\Example::class
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [
        RostersNightsEntryModel::class => NightModel::class,
        RostersRostersEntryModel::class => RosterModel::class,
    ];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [
        NightRepositoryInterface::class => NightRepository::class,
        RosterRepositoryInterface::class => RosterRepository::class,
    ];

    /**
     * Additional service providers.
     *
     * @type array|null
     */
    protected $providers = [
        //\ExamplePackage\Provider\ExampleProvider::class
    ];

    /**
     * The addon view overrides.
     *
     * @type array|null
     */
    protected $overrides = [
        //'streams::errors/404' => 'module::errors/404',
        //'streams::errors/500' => 'module::errors/500',
    ];

    /**
     * The addon mobile-only view overrides.
     *
     * @type array|null
     */
    protected $mobile = [
        //'streams::errors/404' => 'module::mobile/errors/404',
        //'streams::errors/500' => 'module::mobile/errors/500',
    ];

    /**
     * Register the addon.
     */
    public function register()
    {
        // Run extra pre-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Boot the addon.
     */
    public function boot()
    {
        // Run extra post-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Map additional addon routes.
     *
     * @param Router $router
     */
    public function map(VersionRouter $versions)
    {
        // Register dynamic routes here for example.
        // Use method injection or commands to bring in services.

        $versions->route($this->addon, VersionsController::class);
    }
}
