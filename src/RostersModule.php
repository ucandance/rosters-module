<?php namespace Finnito\RostersModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

/**
 * Class RostersModule
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class RostersModule extends Module
{

    /**
     * The navigation display flag.
     *
     * @var bool
     */
    protected $navigation = true;

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'glyphicons glyphicons-calendar';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'rosters' => [
            'buttons' => [
                'new_roster',
            ],
        ],
    ];

}
