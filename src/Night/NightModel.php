<?php namespace Finnito\RostersModule\Night;

use Anomaly\Streams\Platform\Model\Repeater\RepeaterNightsEntryModel;
use Finnito\RostersModule\Night\Contract\NightInterface;

/**
 * Class NightModel
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightModel extends RepeaterNightsEntryModel implements NightInterface
{

    /**
     * Attach the repeater form builder
     *
     * @return null|string
     */
    public function newRepeaterFieldTypeFormBuilder()
    {
        return app(\Finnito\RostersModule\Night\Support\RepeaterFieldType\FormBuilder::class);
    }

    /**
     * Return the date of the roster
     *
     * @return null|string
     */
    public function getDate()
    {
        return $this->night->date->format("l")."<br>".$this->night->date->format("d-m-Y");
    }
}
