<?php namespace Finnito\RostersModule\Night;

use Anomaly\Streams\Platform\Entry\EntryRouter;

/**
 * Class NightRouter
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightRouter extends EntryRouter
{

}
