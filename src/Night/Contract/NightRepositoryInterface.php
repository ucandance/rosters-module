<?php namespace Finnito\RostersModule\Night\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

/**
 * Class NightRepositoryInterface
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
interface NightRepositoryInterface extends EntryRepositoryInterface
{

}
