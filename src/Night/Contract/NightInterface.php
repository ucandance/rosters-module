<?php namespace Finnito\RostersModule\Night\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Class NightInterface
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
interface NightInterface extends EntryInterface
{

}
