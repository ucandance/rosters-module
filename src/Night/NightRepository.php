<?php namespace Finnito\RostersModule\Night;

use Finnito\RostersModule\Night\Contract\NightRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

/**
 * Class NightRepository
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightRepository extends EntryRepository implements NightRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var NightModel
     */
    protected $model;

    /**
     * Create a new NightRepository instance.
     *
     * @param NightModel $model
     */
    public function __construct(NightModel $model)
    {
        $this->model = $model;
    }
}
