<?php namespace Finnito\RostersModule\Night;

use Anomaly\Streams\Platform\Entry\EntryCriteria;

/**
 * Class NightCriteria
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightCriteria extends EntryCriteria
{

}
