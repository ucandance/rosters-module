<?php namespace Finnito\RostersModule\Night;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;

/**
 * Class NightSeeder
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightSeeder extends Seeder
{

    /**
     * Run the seeder.
     */
    public function run()
    {
        //
    }
}
