<?php namespace Finnito\RostersModule\Night\Support\RepeaterFieldType;

/**
 * Class FormBuildr
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class FormBuilder extends \Anomaly\Streams\Platform\Ui\Form\FormBuilder
{
    /**
     * The options for the FormBuilder
     *
     * @return null
     */
    protected $options = [
        "input_view" => "finnito.module.rosters::admin/input_view",
        "form_view" => "finnito.module.rosters::admin/repeater_form",
    ];
}
