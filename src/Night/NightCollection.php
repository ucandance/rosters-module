<?php namespace Finnito\RostersModule\Night;

use Anomaly\Streams\Platform\Entry\EntryCollection;

/**
 * Class NightCollection
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightCollection extends EntryCollection
{

}
