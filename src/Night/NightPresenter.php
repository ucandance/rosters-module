<?php namespace Finnito\RostersModule\Night;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

/**
 * Class NightPresenter
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightPresenter extends EntryPresenter
{

}
