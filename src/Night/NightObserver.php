<?php namespace Finnito\RostersModule\Night;

use Anomaly\Streams\Platform\Entry\EntryObserver;

/**
 * Class NightObserver
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightObserver extends EntryObserver
{

}
