<?php namespace Finnito\RostersModule\Roster;

use Anomaly\Streams\Platform\Entry\EntryCriteria;

/**
 * Class RosterCriteria
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class RosterCriteria extends EntryCriteria
{

}
