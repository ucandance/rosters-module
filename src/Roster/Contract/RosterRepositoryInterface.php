<?php namespace Finnito\RostersModule\Roster\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

/**
 * Class RosterRepositoryInterface
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
interface RosterRepositoryInterface extends EntryRepositoryInterface
{

    /**
     * Get the current roster
     *
     * @return null|RosterInterface
     */
    public function current();

    /**
     * Get all not current rosters
     *
     * @return null|RosterCollection
     */
    public function notCurrent();

    /**
     * Find a roster by its slug.
     *
     * @return null|RosterInterface
     */
    public function findBySlug($slug);
}
