<?php namespace Finnito\RostersModule\Roster\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Class RosterInterface
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
interface RosterInterface extends EntryInterface
{

}
