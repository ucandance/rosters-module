<?php namespace Finnito\RostersModule\Roster;

use Finnito\RostersModule\Roster\Contract\RosterRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

/**
 * Class RosterRepository
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class RosterRepository extends EntryRepository implements RosterRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var RosterModel
     */
    protected $model;

    /**
     * Create a new RosterRepository instance.
     *
     * @param RosterModel $model
     */
    public function __construct(RosterModel $model)
    {
        $this->model = $model;
    }

    /**
     * Get the current roster
     *
     * @return null|RosterInterface
     */
    public function current()
    {
        return $this->model
            ->orderBy('name', 'DESC')
            ->first();
    }

    /**
     * Get all not current rosters
     *
     * @return null|RosterCollection
     */
    public function notCurrent()
    {
        return $this->model
            ->orderBy('name', 'DESC')
            ->skip("1")
            ->get();
    }

    /**
     * Return the roster by its given slug.
     *
     * @param $slug
     * @return null|RosterInterface
     */
    public function findBySlug($slug)
    {
        return $this->model
            ->where("slug", "=", $slug)
            ->first();
    }
}
