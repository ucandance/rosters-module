<?php namespace Finnito\RostersModule\Roster;

use Anomaly\Streams\Platform\Entry\EntryRouter;

/**
 * Class RosterRouter
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class RosterRouter extends EntryRouter
{

}
