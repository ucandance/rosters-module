<?php namespace Finnito\RostersModule\Roster\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

/**
 * Class RosterFormBuilder
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class RosterFormBuilder extends FormBuilder
{

    /**
     * The form fields.
     *
     * @var array|string
     */
    protected $fields = [
        '*',
        'nights' => [
            'input_view' => 'finnito.module.rosters::admin/repeater_input'
        ],
    ];

    /**
     * Additional validation rules.
     *
     * @var array|string
     */
    protected $rules = [];

    /**
     * Fields to skip.
     *
     * @var array|string
     */
    protected $skips = [];

    /**
     * The form actions.
     *
     * @var array|string
     */
    protected $actions = [];

    /**
     * The form buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        "versions",
        'cancel',
    ];

    /**
     * The form options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The form sections.
     *
     * @var array
     */
    protected $sections = [
        "main" => [
            "view" => "finnito.module.rosters::admin.roster_form",
        ],
    ];

    /**
     * The form assets.
     *
     * @var array
     */
    protected $assets = [
        "scripts.js" => [
            "finnito.module.rosters::js/RosterFormHelper.js",
        ],
        "styles.css" => [
            "finnito.module.rosters::css/customForms.css",
        ],
    ];
}
