<?php namespace Finnito\RostersModule\Roster;

use Anomaly\Streams\Platform\Entry\EntryObserver;

/**
 * Class RosterObserver
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class RosterObserver extends EntryObserver
{

}
