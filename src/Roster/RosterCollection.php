<?php namespace Finnito\RostersModule\Roster;

use Anomaly\Streams\Platform\Entry\EntryCollection;

/**
 * Class RosterCollection
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class RosterCollection extends EntryCollection
{

}
