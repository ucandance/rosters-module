<?php namespace Finnito\RostersModule\Roster\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

/**
 * Class RosterTableBuilder
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class RosterTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        "name",
        'updated' => [
            'wrapper'  => '
                <small class="text-muted">{value.author} | {value.ts}</small>
                <br>
                <span>{value.commit}</span>',
            'value' => [
                'author' => 'entry.updated_by.display_name',
                'ts'     => 'entry.updated_at',
                'commit'     => 'entry.commit_message',
            ],
            "heading" => "Last Update",
        ],
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit'
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];
}
