<?php namespace Finnito\RostersModule\Roster;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

/**
 * Class RosterPresenter
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class RosterPresenter extends EntryPresenter
{

}
