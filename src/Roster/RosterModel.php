<?php namespace Finnito\RostersModule\Roster;

use Finnito\RostersModule\Roster\Contract\RosterInterface;
use Anomaly\Streams\Platform\Model\Rosters\RostersRostersEntryModel;

/**
 * Class RosterModel
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class RosterModel extends RostersRostersEntryModel implements RosterInterface
{

}
