<?php namespace Finnito\RostersModule\Roster;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;

/**
 * Class RosterSeeder
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class RosterSeeder extends Seeder
{

    /**
     * Run the seeder.
     */
    public function run()
    {
        //
    }
}
