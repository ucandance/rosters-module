<?php namespace Finnito\RostersModule\Roster\Command;

use Finnito\RostersModule\Roster\Contract\RosterInterface;
use Anomaly\Streams\Platform\View\ViewTemplate;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Class AddTemplateEditLink
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class AddTemplateEditLink
{

    /**
     * The roster
     */
    protected $roster;

    /**
     * Construct a new AddTemplateEditLink instance.
     *
     * @param RosterInterface $roster
     * @return AddTemplateEditLink
     */
    public function __construct(RosterInterface $roster)
    {
        $this->roster = $roster;
    }

    /**
     * Handle the AddTemplateEditLink command.
     *
     * @return null
     */
    public function handle(ViewTemplate $template)
    {
        $template->set('edit_link', "/admin/rosters/edit/".$this->roster->id);
        $template->set("edit_type", $this->roster->name . " Roster");
    }
}
