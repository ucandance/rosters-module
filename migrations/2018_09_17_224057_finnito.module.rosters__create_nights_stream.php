<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleRostersCreateNightsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'nights',
        'namespace'    => 'repeater',
        'title_column' => 'night_id',
        'translatable' => false,
        'trashable' => false,
        'searchable' => false,
        'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        "night",
        "door_1",
        "door_2",
        "sound_1",
        "sound_2",
        "mj_1",
        "mj_2",
        "latin_1",
        "latin_2",
    ];

}
