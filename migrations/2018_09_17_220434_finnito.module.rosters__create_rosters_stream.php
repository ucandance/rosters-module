<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleRostersCreateRostersStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'rosters',
         'title_column' => 'name',
         'translatable' => false,
         'trashable' => false,
         'searchable' => false,
         'sortable' => true,
         "versionable" => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name' => [
            'translatable' => true,
            'required' => true,
        ],
        'slug' => [
            'unique' => true,
            'required' => true,
        ],
        "year",
        "nights",
        "commit_message" => [
            "required" => true,
        ]
    ];

}
