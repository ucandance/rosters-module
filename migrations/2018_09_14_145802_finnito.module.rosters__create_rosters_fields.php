<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;
use Finnito\ClassesModule\Night\NightModel;
// use Anomaly\Streams\Platform\Model\Repeater\RepeaterNightsEntryModel;
use Finnito\RostersModule\Roster\RosterModel;
use Anomaly\UsersModule\User\UserModel;

class FinnitoModuleRostersCreateRostersFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'name' => 'anomaly.field_type.text',
        'slug' => [
            'type' => 'anomaly.field_type.slug',
            'config' => [
                'slugify' => 'name',
                'type' => '-'
            ],
        ],
        "year" => [
            "type" => "anomaly.field_type.integer",
            "config" => [
                "separator" => "",
            ],
        ],
        "nights" => [
            "type" => "anomaly.field_type.repeater",
            "locked" => false,
            "config" => [
                "related" => \Finnito\RostersModule\Night\NightModel::class,
                "add_row" => "finnito.module.rosters::button.add_class",
            ],
        ],
        "night" => [
            "type" => "anomaly.field_type.relationship",
            "namespace" => "repeater",
            "config" => [
                "related" => \Finnito\ClassesModule\Night\NightModel::class,
                "mode" => "lookup",
                "lookup_table" => \Finnito\ClassesModule\Night\Support\RelatedFieldType\LookupTableBuilder::class,
                "selected_table" => \Finnito\ClassesModule\Night\Support\RelatedFieldType\SelectedTableBuilder::class,
                "value_table" => \Finnito\ClassesModule\Night\Support\RelatedFieldType\ValueTableBuilder::class,
            ],
        ],
        "door_1" => [
            "type" => "anomaly.field_type.relationship",
            "namespace" => "repeater",
            "config" => [
                "related" => UserModel::class,
                "mode" => "search",
            ],
        ],
        "door_2" => [
            "type" => "anomaly.field_type.relationship",
            "namespace" => "repeater",
            "config" => [
                "related" => UserModel::class,
                "mode" => "search",
            ],
        ],
        "sound_1" => [
            "type" => "anomaly.field_type.relationship",
            "namespace" => "repeater",
            "config" => [
                "related" => UserModel::class,
                "mode" => "search",
            ],
        ],
        "sound_2" => [
            "type" => "anomaly.field_type.relationship",
            "namespace" => "repeater",
            "config" => [
                "related" => UserModel::class,
                "mode" => "search",
            ],
        ],
        "mj_1" => [
            "type" => "anomaly.field_type.relationship",
            "namespace" => "repeater",
            "config" => [
                "related" => UserModel::class,
                "mode" => "search",
            ],
        ],
        "mj_2" => [
            "type" => "anomaly.field_type.relationship",
            "namespace" => "repeater",
            "config" => [
                "related" => UserModel::class,
                "mode" => "search",
            ],
        ],
        "latin_1" => [
            "type" => "anomaly.field_type.relationship",
            "namespace" => "repeater",
            "config" => [
                "related" => UserModel::class,
                "mode" => "search",
            ],
        ],
        "latin_2" => [
            "type" => "anomaly.field_type.relationship",
            "namespace" => "repeater",
            "config" => [
                "related" => UserModel::class,
                "mode" => "search",
            ],
        ],
        "commit_message" => [
            "type" => "anomaly.field_type.textarea",
        ],
    ];

}
